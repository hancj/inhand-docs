# 开发流程规范

## git flow 工作流

![git flow](https://ps.w.org/documents-from-git/assets/icon-256x256.png?rev=2274827g)
由上图可知，ｇit flow 主要包含了两个分支　 develop，master。其中 master 分支的每一个版本都存在 tag，表示一个可运行版本。在 git flow 工作流中，需要注意一下几点：

1. develop 分支基于 master 分支。
2. 每个 feature 分支，都应该基于 develop 分支。
3. 各个 feature 分支间不需要处理冲突，因为每个feature相对功能独立，任务单一，只和develop有关系，请求合并到develop前需要处理冲突，然后审核，最后合并。
4. 每次需要发布生产版本时，由develop生成分支 release 分支，。**一旦打了Release分支之后不要从Develop分支上合并新的改动到Release分支**，且不能再在release分支上增加功能，release分支只修复的bug。
5. release 分支修复bug并测试通过后，通过 pull request 合并到 master 分支和develop分支，并且 master 在这时打上标签 xxx（版本号）。此时应该删除Release分支。
6. 当master分支出现bug后，应复制出 hotfix分支，热修复并测试通过后pull request到master和develop 分支。
7. feature分支表示一个独立的功能模块,可能会经历1到多个sprint才能开发完成.
8. feature分支或release分支里在sprint中会被分成多种工作类型开发分支,开发时从源分支复制,开发完成后合并回源分支
8.1 story表示用户故事
8.2 task表示jira中的story或task下的sub task
8.3 fix 表示修改bug
8.4 refactor 表示重构,UI改进,但功能和业务逻辑并未改变
8.5 chore表示ci或支持库更新,但并未修改功能或业务逻辑


**注意：如果同时开发两个比较大的版本，则应维护两个master和develop，比如 master 和master-a，develop和develop-b。如python 2.x和python 3.x同属于python，但差别很大**


## 软件版本命名规范

  软件版本分为开发版本,预览(Release)版本和生产(Product)版本。



- 开发版本: 研发组内部的版本,一般以develop分支上的commit id未标识,由于采用scrum及迭代开发,develop分支更新很快,每天可能会有多个更新部署到开发服务器上
- 预览版本:对应生产版本的待测试版本.分支名采用 release/r{a}.{b}.{c}方式,版本显示为采用r{a}.{b}.{c}。当在jira中规划的这个版本所包括的(可能会包含一个或多个sprint的story)story都开完成并初步通过develop阶段的测试后,即可以从develop分钟哦复制一份到新建的release/r{a}.{b}.{c}上,并从这个新分支部署版本到预发布测试服务器上测试.由于规划版本哦时功能范围已经确定,不再接受来自develop分支的合并,

- 生产版本:正常情况是按照软件常规命名，也就是v{a}.{b}.{c}.预览版本测试通过并通过评审后,合并release到develop和master分支,并在master分支打上v{a}.{b}.{c}的tag.此时CI部署到生产服务器上,并自动生成v{a}.{b}.{c}的版本呢标识
## 新功能开发流程

下图是一个开发示意图：

![开发流程](images/gitflow_0.jpg)

基于 git flow 工作流程，开发过程中，一个 sprint中有多个story或sub-task,每个story或sub-task就是一个 feature，原则上粒度到story即可。下面是以 sprint9 中的story：IOV-288为例，介绍开发过程中 git 操作：

### 开启 sprint

​       每个sprint启动时，会包含多个story，而每个story可能有0~n个sub task.这些story或sub-task可能属于某个版本。

​       每个story或sub-task是一个独立的开发循环，包括：开始->新建功能分支->实现->内部测试->代码提交->请求合并->review及构建验证->合并->测试->关闭功能分支->结束。

一个开发循环涉及四个角色：

- 开发者
- 审核者
- CI系统
- 测试者

​       一个开发循环为以下过程：

![功能开发流程](images/gitflow_1.jpg)



#### 切换到 develop 分支

```bash
git checkout develop
```

#### 创建 feature/IOV-288 

```bash
git checkout -b feature/IOV-288
```



### 周期内开发过程

开发人员开发IOV-288的功能或者修复bug，内部测试完成后，然后将代码 pull request 到 develop分支.

### 开发周期结束处理

这时， 从feature-IOV到develop 分支上的pull request必须通过另一个开发同事的代码Review以及CI服务器的编译验证，如果二者通过，则申请者可点击合并按钮到develop分支。

此时develop分支会自动发布到开发服务器上，负责测试验证的同事收到通知，开始针对story的测试。

以上就是一个 feature 开发流程。

而一个sprint中包含多个story，每提交一个feature/IOV-${story index}，就可以进行一次针对该story的测试。直到该sprint中的story都测试完。

*注意：*

* jira中定义一个版本应以{a}.{b}.{c}

- 产品Release预发布版本和产品Product版本是两种版本，
- Relase版本是内部开发标识即将上线但未完全测试完毕并通过的版本, 一般为'r{a}.{b}.{c}','r'表示release。
- 产品product版本用来标识正式的产品版本，一般为‘v{a}.{b}.{c}','v'表示version*



## 发布一个产品版本-Product版本 

当规划了一个产品版本，其包括的多个 sprint和story 开发并测试通过后，开始发布Release预览版本。操作如下：

### 开始一个预览版本-Release版本

在发布之前，将 develop 分支与代码仓库同步。然后执行

```bash
 git  checkout develop
 git  pull
 git  checkout -b release/${产品版本号}
```

操作完成后，比如将 release/r1.0.1 推送到代码库中。
注意：r1.0.1 是产品Release版本号。

当产品releaes版本测试通过后，即可将其合并会master分支和develop分支，并在master分支上生成tag，并发布生产版本



下面介绍 release/xx 如何合并到 master 分支上。

#### release 分支无Bug

当 develop 创建的 release/${产品版本号} 分支后，测试没有发现没问题，也没有修改，可以直接合并到 master 分支。这时只需要进行如下操作。

1. 分别发起一个从 release/xxx pull request 到 master和develop分支
2. 合并代码，并且在 master 分支打上一个 tag 　**v{a}.{b}.{c}** (版本号)。
3. 删除release分支

#### release 分支有Bug

创建分支 release/${产品版本号}，发现存在 bug，需要修复，直接在release分支上修复bug，但不允许添加新功能。建议开发者最好复制该分支来新建本地开发分支，本地测试通过后，发送pull request，请求review后合并到该release分支.

**注意: 修复relase分支bug时应从release分支复制新分支,分支名应以bug开头.**

> 格式: bug/IOV-xxx
>
> - 'bug'表示修复bug
> - 'IOV'为项目缩写
> - '/'和'-'为分隔符
> - 'xxx'为story或sub task的id**

## 热修复

下述情况需要创建 hotfix 分支:

- master 版本中发现了 bug 或者漏洞

在热修复前，需要保证操作机器和代码仓同步。执行如下：

### 开启一个热修复

假设修复 BUG 后的版本从 v1.0.1 至 v1.0.2

```bash
git checkout master
git checkout v1.0.1
git checkout -b hotfix/v1.0.2
```

然后将分支上传到代码库或者直接在gitlab/github上直接建立hotfix分支然后pull下来前换到该hotfix分支。

### 修复开发

开发者在**hotfix/v1.0.2**分支开发并通过测试部同事测试后,从而结束 bug 修复。

### 合并热处理

修复工作结束后，确认代码无误，同步代码仓。发起**hotfix/v1.0.2**到 master 的 pull request。并且打上 tag 　 v1.0.2。

再发起**hotfix/v1.0.2**到 develop 的 pull request。合并完成。
至此，热修复工作完成。