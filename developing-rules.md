# API模块开发准则(2019-07-04)

## 一. 概述

本文描述基于InHand DeviceNetworks Cloud - elements架构的项目开发准则

## 二. 模块项目开发

### 2.1 概述



### 2.2 SpingCloud API模块

#### 2.2.1 项目模板

#### 2.2.2 maven私有仓库

私有仓库用来存储inhand开发的公用或业务转用支持库文件.如: cloud-lib,smartfleet-common-lib

私有仓库地址:  maven { url "http://nexus.inhand.design/repository/maven-public" }

一般存放两种版本:

- release预发布版本: 库形式: ${project}-x.y.z-SNAPSHOT,如

  ```
  implementation("cn.com.inhand.cloud.smartfleet:smartfleet-common-lib:1.3.0-SNAPSHOT")
  ```

- 生产版本:对应git仓库中打上tag: x.y.z的版本,库形式: 如: ${project}-x.y.z

  ```
  implementation("cn.com.inhand.cloud.smartfleet:smartfleet-common-lib:1.3.0")
  ```

当在开发过程中,开发的api项目引用预发布版本,但预发布版本有功能更新或修复bug更新了库,但在release阶段并未更新库版本号时,则在构建api项目时手动删除本地缓存的文件:

![](images/misc/del-same-version-lib.jpg)

编译时执行

~~~shell
 gradle build --refresh-dependencies
~~~





#### 2.2.3 gradle脚本,CI脚本及辅助脚本

- build.gradle
- git_info.py
- build_docker_java_app.sh
- DockerFile
- .gitinore
- .gitlab-ci.yml

#### 2.2.4 监控支持

##### 2.2.4.1 git information支持

参阅[](https://plugins.gradle.org/plugin/com.gorylenko.gradle-git-properties)

build.gradles文件中:

```groovy
    implementation("cn.com.inhand.cloud.services:elements-common:4.0.1-SNAPSHOT")
    implementation("cn.com.inhand.cloud.services:cloud-lib:4.0.1-SNAPSHOT")
    implementation("cn.com.inhand.cloud.services:oauth-lib:4.0.1-SNAPSHOT")
```

**原来的这些库需要替换为以下库**

    implementation("cn.com.inhand.cloud.common:elements-common:2.0.0.129be0-SNAPSHOT") #及以后版本,本库可选,只在车载项目中是必选
    implementation("cn.com.inhand.cloud.common:cloud-lib:2.3.0.a583ff-SNAPSHOT")   #及以后版本
    implementation("cn.com.inhand.cloud.common:oauth-lib:1.0.0") #及以后版本


##### 2.2.4.2 依赖库版本

加入依赖库:

build.gradles文件中:

```groovy
plugins {
  id "com.gorylenko.gradle-git-properties" version "2.0.0"
}
...
buildscript {
    repositories {
        maven { url "http://maven.aliyun.com/repository/spring-plugin" }
        maven { url "http://maven.aliyun.com/repository/gradle-plugin" }
    }
    dependencies {
        classpath 'org.springframework.build.gradle:propdeps-plugin:0.0.6'
        classpath "org.springframework.boot:spring-boot-gradle-plugin:2.1.3.RELEASE"
        classpath "com.sarhanm:gradle-versioner:3.0.9"
        classpath "gradle.plugin.com.gorylenko.gradle-git-properties:gradle-git-properties:2.0.0"
    }
}


apply plugin: "com.gorylenko.gradle-git-properties"
```

##### 

##### 2.2.4.3 查看所运行版本

两种方式:

- zookeeper查看/element/apps下相应节点数据

  ![](images/misc/zk-node-version.png)

- 通过traefik代理查看

  GET http://${server}:7000/cloud/apps/${模块名} 

  如: http://smartfleet.inhandiot.com:7000/cloud/apps/nm-ext

  返回: 

~~~json
{
	"name": "nm-ext-1",
	"date": "2019-07-04T09:54:16.108Z",
	"startupAt": "2019-07-04T09:19:08.080Z",
	"status": "UP",
	"id": "ebca5018-8da3-45ce-966b-5b7ca2d8578c",
	"registAt": "2019-07-04T09:19:14.968Z",
	"info": {
		"git": {
			"build": {
				"host": "guangzhiyingdeMacBook-Pro.local",	
				"version": "",
				"time": 1562175040,
				"user": {
					"name": "hancj-dev",
					"email": "13910083973@139.com"
				}
			},
			"branch": "refactor-api-doc",
				"commit": {
				"message": {
				"short": "refactor: support monitor",
				"full": "refactor: support monitor\n"
			},
			"id": {
        "describe": "",
        "abbrev": "15a8f13",
        "full": "15a8f132d91d2d612b07c5ef4df08fdd4a8217d5"
      },
      "time": 1562149779,
      "user": {
        "email": "13910083973@139.com",
        "name": "hancj-dev"
      }
    },
    "closest": {
      "tag": {
        "name": "",
          "commit": {
          "count": ""
        }
      }
    },
    "dirty": "true",
    "remote": {
      "origin": {
        "url": "http://gitlab.inhand.design/SmartFleet/nm-ext.git"
      }
    },
    "tags": "test-tag",
    "total": {
      "commit": {
        "count": "5"
      }
    }
  },
	"build": {
			"version": "",
			"artifact": "nm-ext",
			"name": "nm-ext",
			"group": "cn.com.inhand.cloud.smartfleet",
			"time": 1562231452.075
		}
	}
}

~~~

##### 2.2.4.4 获取swagger json
使用有效账号登录yapi: http://10.5.16.213:3000
新建或编辑已存在api项目名称,然后输入要测试的api模块
![](images/yapi/main.png)
以basic模块为例
进入该模块,
![](images/yapi/main.png)
初次或要更新api文档时,在"数据管理页面"导入swagger json
1.对于未升级到cloud-lib:2.3.0.{commit id}-SNAPSHOT版本以后的api项目,由于traefkic并未代理swagger json url,只能从本地开发环境或k8s上导入,地址如下: 
~~~
http://{dev server}:{api port}/v2/api-docs?group={api模块名}
~~~
如: http://10.5.16.213:8090/v2/api-docs?group=basics

2.对于已升级到cloud-lib:2.3.0.{commit id}-SNAPSHOT版本以后的api项目,可以直接从相应平台上导入,地址如下:
~~~
http://{release server}/cloud/apps/{api模块名}/v2/api-doc
~~~
如: http://smartfleet.inhandiot.com/cloud/apps/message-ext/v2/api-doc

导入swagger:
![](images/yapi/import-swagger-url.jpg)
可以设置定时同步swagger json
![](images/yapi/schedule-setting.jpg)

可以设置多个目标测试服务器环境

![](images/yapi/env-setting.jpg)
然后就可测试api了
![](images/yapi/api-testing.jpg)

#### 2.2.5 module/vo开发规范
对于已升级到cloud-lib:2.3.0.{commit id}-SNAPSHOT版本以后的api项目,开发module/VO类时,可以在对应数据库字段上加入@QueryVerbose注解
~~~
@Data
public class Modules {
    ObjectId id;
    ObjectId oid;
    String name;
    String ype;
    ObjectId deviceId;
    Integer online;
    @QueryVerbose(value=100)
    Map<String,Object> properties;
    @QueryVerbose(value=100)
    Date createAt;
    @QueryVerbose(value=100)
    Date stateUpdatedTime;
}
~~~
实现详情或列表api时,可以加入可选查询参数: verbose, 此时可以自行实现或用AbstractQueryFactory生成只返回小于该verbose值,或没有@QueryVerbose的Query类
参考项目: http://gitlab.inhand.design/dn/site-ext.git 中SiteExtService.java的findSites(...)函数

#### 2.2.6 查询列表参数规范
提供查询列表时,出提供业务相关的filter查询条件外,应提供规范的分页,排序参数
- 分页参数:  
  - cusor
  - limit
- 排序参数: 
  - orderby: 排序字段,必须与数据库中的字段名相同,多个拍讯用逗号分隔
  - desc: 是否降序
- 详细参数:
  - verbose: 如果设置该值,则每个记录只返回无@QueryVerbose注解或小于@QueryVerbose值的字段
参数范例:
~~~
AdvancePageResult<Site> findSites(AuthInfo auth,
                                      ...
                                      @RequestParam(value = "verbose", defaultValue = "10") Integer verbose,
                                      @RequestParam(value = "cursor", defaultValue = "0") Integer cursor,
                                      @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                      @RequestParam(value = "orderby" ,required = false) String orderby,
                                      @RequestParam(value = "desc" ,required = false) Boolean desc)throws Exception {
                                        ...
~~~  
#### 2.2.7 
参考: cloud-lib项目源码, cn.com.inhand.cloud.persistence和cn.com.inhand.cloud.persistence.mongo是基于mongodb的便捷开发实现
- QueryVerbose: 实现Module/VO类的注解
- AbstractQueryFactory:  QueryFactory是具体实现
  - 根据Map<String,Object>参数生成Query
  - 根据Map<String,Object>生成Update
- BaseDaoService: 基于反射和泛型实现的多mdouel的mongo的crud框架
- MongoDaoService: 基于反射泛型实现的针对某个module的mongo的crud框架
参考: https://gitlab.inhand.design/dn/group-ext,此项目是基于此框架实现的api和service


### 2.3. 前端项目

### 2.4. Python项目模块

## 三. 运行环境

### 3.1 镜像仓库

### 3.2 docker-compose方式

### 3.3 K8s方式 

## 四、API文档中心

## 五、测试

