# 生产环境部署步骤(2019-06-13)

##  一. 概述

本文档描述如何在生产环境部署车载平台新版本.

生产环境分为:

- 国内平台:  che.inhandiot.com
- 国外平台: www.smartfleet.cloud

生产环境部署只能由运维部部署.

配置环境存储仓库:  <https://gitlab.inhand.design/SmartFleet/configuration>

该仓库的tags对应发布版本的版本.如: tag=v1.0.0-che.inhandiot.com表示che.inhandiot.com的v1.0.0部署配置.

![配置](images/deploy/gitlab-conf-tags.png)

部署策略为:

1. PO提交[<<部署计划>>](docs/deploying/deploying_schedule.docx),内容包含目标部署平台,预期上线时间,上线模块各部分版本tag.docker镜像版本,本次版本更新功能,数据库升级,升级操作指令,注意事项等;
2. 运维部实施部署.完毕后,在<<部署计划>>中签字,标注确认结果.上线日期;
3. 测试部根据<<部署计划>>验证平台上线版本,确认后签字,标注确认结果.

**注意: 生产服务器采用证书登录,且只有运维部能登录.**

## 二 . 国内平台部署

### 2.1 部署步骤

运维部在接收到<<部署计划>>后,根据文档内容,实施部署,部署内容在本项目中包括:

- 备份当前版本环境
- 部署配置: 下载配置,并上传到生产服务器,解压,替换掉config目录
- 部署前端: 去Release服务器下载指定tag的前端打包文件,上传至生产服务器,解压替换掉 /data/elements/html目录
- 升级数据库: 根据<<部署计划>>中提示的升级操作指令,如包含升级数据库,则按照其指定的数据库脚本或源数据库对生产平台数据库进行升级
- 执行升级指令

### 2.2 备份当前环境

备份/data/elements/config目录.对各虚拟机制作快照.

### 2.3 部署配置

步骤:

开发环境

- 进入smartfleet.inhandiot.com服务器 cd /data/elements/config 目录
- 使用git命令  git add .  和   git commit -m  提交本次sprint的修改 （使用 git diff 或者 git status 可以查看更改）
- 使用 tar -czf config-r{X}.{Y}.{Z}.tgz 命令，将config目录打包
- 将压缩包下载至本地，创建configuration临时分支rel/r{X}.{Y}.{Z}，并push压缩包内文件至远程仓库,合并分支到release分支。以release分支打tag，release-r{X}.{Y}.{Z}
- 查看差异: <https://gitlab.inhand.design/SmartFleet/configuration/compare?from=product%2Fche.inhandiot.com&to=product%2Fche.inhandiot.com><<部署计划>>中列出的tag版本.并查看和记录该版本较上一tag差异文件.
- 以product/che.inhandiot.com新建分支che-v{X}.{Y}.{Z},以release版本差异为基础，修改che-v{X}.{Y}.{Z}分支文件，确认无误后合并至product/che.inhandiot.com分支
- 以release 分支新建 rel/{X}.{Y}.{Z}，clone新分支到本地，使用命令 tar -czf config-r{X}.{Y}.{Z}.tgz config 打包并上传到  smartfleet.inhandiot.com
- 执行命令: mv /data/elements/config /data/elemnts/config-{原来的开发版本号}
- 命令 tar -xzf config-r{X}.{Y}.{Z}.tgz 解压新的config文件，开始新的sprint

生产环境 （生产环境由运维操作）

- 到配置仓库克隆: [git http://gitlab.inhand.design/SmartFleet/configuration.git](http://gitlab.inhand.design/SmartFleet/configuration.git).clone成功后,将该文件压缩为configuration-v{X}.{Y}.{Z}-che.inhandiot.com.tar.gz.
**注意:请用linux系统操作,避免回车换行带来的文件差异.**

![download-file](images/deploy/download-gitlab-conf-tags.jpg)

- 上传该压缩文件(configuration-v{X}.{Y}.{Z}-che.inhandiot.com.tar.gz)到生产服务器. 
- 将压缩文件(configuration-v{X}.{Y}.{Z}-che.inhandiot.com.tar.gz)解压到临时目录下
- 执行命令: mv /data/elements/config /data/elemnts/config-{原来的生产版本号},并确认是否切换成功: git branch 列出当前的分支或tag是否为'v{X}.{Y}.{Z}-che.inhandiot.com'
- 进入临时目录,切换到tag: git checkout v{X}.{Y}.{Z}-che.inhandiot.com 
- 在临时目录下的config目录改为 /data/elements/config
- 配置完成
  - 如果失败,则立即回滚.
  - 如果成功,则压缩该文件夹 tar xzvf config-v{X}.{Y}.{Z}-che.inhandiot.com-deployed.tgz /data/elements/config.将压缩后的文件下载并提交给PO
此时,部署完成

### 2.4 部署前端

步骤:

- 登录https://gitlab.inhand.design/SmartFleet/SmartFleet_UI/pipelines,找到对应tag的构建,如v1.2.1

  ![](images/deploy/how2download_frontend.png)

  将下载文件命名为: smartfleet_ui.v{X}.{Y}.{Z}.zip

- ssh inhand@smartfleet.inhandiot.com. 将刚下载的smartfleet_ui.v{X}.{Y}.{Z}.zip文件上传至che.inhandiot.com.

- 备份/data/elements/html/SmartFleet

- 将smartfleet_ui.v{X}.{Y}.{Z}.tgz解压/data/elements/html/SmartFleet下.

- 部署前端完成

### 2.5 部署后端

步骤:

- 到修改过的对应模块，将release分支合并到develop分支和master分支
- 将master分支打tag，命名 v{X}.{Y}.{Z}
- 部署后端完成
- 注意：新的sprint开启时，新进后端模块的release分支，需要到服务器/data/elements/config/docker修改compose.yml文件

### 2.6 升级数据库

按照<<部署计划>>列出的数据库升级要求(如果有内容)中指定的内容,执行指定脚本(该脚本必须在该版本的config目录中)或对Release服务器的指定数据库进行表复制.如果失败,则立即回滚.

### 2.7 执行升级指令

按照<<部署计划>>列出的执行升级指令要求(如果有内容)中指定的内容,执行指定脚本(该脚本必须在该版本的config目录中).如果正常,则进入下一阶段,如果失败,则立即回滚.

执行指令包括但不限于

- 重新加载docker指令: docker-compose.yml文件一般会有变动,如新增docker镜像,或docker镜像版本升级
- 启动或重启某个中间件: 新增某个基于docker的中间件或某个中间件需要加载新配置

### 2.8 升级完毕

执行以上操作后,运维部操作人员应从系统层面检查虚拟机,docker,中间件是否正常:

- 如果正常则部署工作完毕.签字.提交<<部署计划>>给PO
- 如果异常则通过升级前备份的快照恢复到原有版本



## 三. 国外平台部署

暂缺,待补充.





